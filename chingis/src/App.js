import React from 'react';
import { SignIn, SignUp } from './pages';
import {HomeDefault} from './pages/Home-default'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { AuthUserProvider } from './providers/auth-user-provider';

const App = () => {
    return (
        <AuthUserProvider>
            <Router>
                <Switch>
                    <Route path="/login">
                        <SignIn/>
                    </Route>
                    <Route path="/register">
                        <SignUp/>
                    </Route>
                    <Route path="/">
                        <HomeDefault/>
                    </Route>
                </Switch>
            </Router>
        </AuthUserProvider>
    )
}

export default App