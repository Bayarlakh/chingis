import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useFirebase } from '../Hooks/firebase';
import { FormInput, Button } from '../components/index'

export const SignUp = () => {
    const [state, setState] = useState({ email: '', password: '', password2: '' });
    const history = useHistory();
    const [error, setError] = useState('');
    const { firebase, auth, firestore } = useFirebase();

    const handleChangeUsername = (e) => setState({ ...state, username: e.target.value })
    const handleChangeEmail = (e) => setState({ ...state, email: e.target.value });
    const handleChangePassword = (e) => setState({ ...state, password: e.target.value });
    const handleChangePassword2 = (e) => setState({ ...state, password2: e.target.value });

    const logout = () => {
        auth.signOut();
    }

    const signUp = async () => {
        if (!(state.email && state.password && state.password2)) {
            setError('Please enter all the required information');
            return;
        }
        if (state.password !== state.password2) {
            setError('Passwords dont match!');
            return;
        }
        let cred = await auth.createUserWithEmailAndPassword(state.email, state.password)            
        .catch((error) => {
            alert(error.message)
        })
        
        await firestore.collection('users').doc(cred.user.uid).set({
            username: state.username,
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        });
        history.push('/')
    }
    
    return (
        <div>
            <p>Нэвтрэх</p>
            <FormInput label='Хэрэглэгчийн нэр' type='text' placeholder='Username' value={state.username} onChange={handleChangeUsername} />
            <FormInput label='Цахим хаяг' placeholder='name@mail.domain' type='email' value={state.email} onChange={handleChangeEmail} />
            <FormInput label='Нууц үг' placeholder='Password' type='password' value={state.password} onChange={handleChangePassword} />
            <FormInput label='Нууц үгээ давтна уу?' placeholder='Password' type='password' value={state.password2} onChange={handleChangePassword2} />
    
            {error && <div>{error}</div>}
            <Button onClick={signUp}>Бүртгүүлэх</Button>
            <Button onClick={logout}>Logout</Button>
    
        </div>
    )
}
