import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../providers/auth-user-provider';
import { useFirebase } from '../Hooks/firebase';
import { FormInput, Button } from '../components/index'

export const SignIn = () => {
    const history = useHistory();
    const { ready, user } = useContext(AuthContext);
    const { auth } = useFirebase();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const signUpPage = () => {
        history.push('./register')
    }
    console.log(user);

    if (user) {
        history.push('/')
    }

    const handleChangeEmail = (e) => setEmail(e.target.value);
    const handleChangePassword = (e) => setPassword(e.target.value);


    const signIn = async () => {
        await auth.signInWithEmailAndPassword(email, password).catch((error) => {
            alert(error.message);
        })
    }

    return (
        <div>
            <p>Нэвтрэх</p>
            <FormInput label='Цахим хаяг' placeholder='name@mail.domain' value={email} onChange={handleChangeEmail} />
            <div className='mt-4'></div>
            <FormInput label='Нууц үг' type='password' placeholder='Password' value={password} onChange={handleChangePassword} />
            <Button onClick={signIn}>Нэвтрэх</Button>
        </div>
    )
}
